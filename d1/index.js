console.log("Hello World");


	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator is " + sum);
	let difference = x - y;
	console.log("Result of subtraction operator is " + difference);
	let product = x * y;
	console.log("Result of multiplication operator is " + product);
	let quotient = x / y;
	console.log("Result of division operator is " + quotient);
	let remainder = y % x;
	console.log("Result of modulo operator is " + remainder);

	// Assignment operators

	let assignmentNumber = 8;

	assignmentNumber = assignmentNumber +2;
	console.log("Result of addition operator: " + assignmentNumber);

	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);

	// mdas and pemdas

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas); 

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: " + pemdas);

	// Increment and Decrement

	let z = 5;
	let increment = ++z;
	console.log("result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);

	let decrement = --z;
	console.log("result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	decrement = z--;
	console.log("result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);


	// Coercion

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;


	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);

	// comparison operators

	let juan = 'juan';

	// equality operator (==)

	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(0 == false);
	console.log('juan' == 'juan');
	console.log("juan" == juan);

	// Inequality Operator

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	console.log('juan' != 'juan');
	console.log("juan" != juan);

	// Strict Equality Operator

	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log("juan" === juan);	

	// Strict Inequality Operator

	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== false);
	console.log('juan' !== 'juan');
	console.log("juan" !== juan);	

	// Relational Operators

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	let isLessThan = a < b;
	let isGTorEqual = a >= b;
	let isLTorEqual = a <= b;

	console.log(isGreaterThan);
	console.log(isLessThan);
	console.log(isGTorEqual);
	console.log(isLTorEqual);

	let numStr = "30";
	console.log( a > numStr);
	console.log( b <= numStr);

	let str = "twenty";
	console.log(b >= str);


	// Logical Operator

	// && - AND

	let isLegalAge = true;
	let isRegistered = false;

	let allRequirementsMet = isLegalAge && isRegistered;
	console.log('Result of logical AND Operator ' + allRequirementsMet);


	// || - OR

	let someRequirementstMet = isLegalAge || isRegistered;
	console.log('Result of logical AND Operator ' + someRequirementstMet);

	// ! - NOT

	let someRequirementsNotMet = !isRegistered;
	console.log('Result of logical AND Operator ' + someRequirementsNotMet);


